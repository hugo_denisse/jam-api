# Jam-api

This is the API used by the Jam client.

It is responsible for serving informations about the songs availables.

## Installation

Node, Npm and a Mongodb server are required.

```sh
git clone https://bitbucket.org/hugo_denisse/jam-api
cd jam-api
npm install
```

Install `nodemon` if not already installed (used by the start script)

```sh
npm install -g nodemon
```

To launch the server, type:

```sh
npm start
```

## Users authentication

Users authentication is made through the [Toctoc](https://bitbucket.org/hugo_denisse/toctoc) microservice.

All POST requests to /api/auth will be proxied to this microservice.

## Environment variables

| Name       | Description                         | Default value         |
| ---------- | ----------------------------------- | --------------------- |
| TOCTOC_URL | url of the toctoc microservice      | http://localhost:8080 |
| DB_NAME    | name of the mongo database          | jam                   |
| PORT       | port on which is running the server | 3001                  |
| JWT_SECRET | secret used to encode JWT           | test                  |
