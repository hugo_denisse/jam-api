const config = require("./config");
const configDatabase = require("./config/database");
const configServer = require("./config/server");

configDatabase();
configServer().listen(config.PORT, () => {
  console.log(`server listening on port ${config.PORT}`);
});
