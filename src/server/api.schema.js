const Joi = require("joi");

const setScoreSchema = Joi.object().keys({
  song: Joi.string()
    .guid()
    .required(),
  score: Joi.number()
    .integer()
    .min(0)
    .required(),
  streak: Joi.number()
    .integer()
    .min(0)
    .required(),
  percent: Joi.number()
    .min(0)
    .max(1)
    .required()
});

module.exports = {
  setScoreSchema
};
