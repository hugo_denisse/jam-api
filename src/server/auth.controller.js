const requestify = require("requestify");
const TOCTOC_URL = require("../config/auth").TOCTOC_URL;

const redirect = (req, res) => {
  if (req.method !== "POST") {
    return res.status(400).json({ message: "Bad Request" });
  }
  return requestify
    .post(TOCTOC_URL + req.url, {
      ...req.body,
      from: "jam"
    })
    .then(response => {
      return res.status(response.code).json(response.getBody());
    })
    .catch(err => {
      if (err.code && err.headers && err.body) {
        return res
          .status(err.code)
          .header(err.headers)
          .send(err.body);
      } else {
        return res.status(503).json({ message: "Service Unavailable" });
      }
    });
};

module.exports = redirect;
