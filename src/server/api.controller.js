const mongoose = require("mongoose");
const Joi = require("joi");
const Song = require("../models/song");
const Score = require("../models/score");
const joiSchemas = require("./api.schema");

function getSongsList(req, res) {
  console.log("get /api/songs");
  return Song.find({ visible: true }, "uuid name").then(songs => {
    res.json({ songs });
  });
}

function getSongMeta(req, res) {
  console.log("get /api/song/" + req.params.name);
  return Song.findOne({ name: req.params.name }).then(song => {
    if (song) {
      res.json(song);
    } else {
      res.status(404).json({ message: "Song not found" });
    }
  });
}

function setScore(req, res) {
  console.log("post /api/score");
  const requestData = Joi.attempt(req.body, joiSchemas.setScoreSchema);

  const score = new Score({
    user_uuid: req.user.uuid,
    song_uuid: requestData.song,
    score: requestData.score,
    streak: requestData.streak,
    percent: requestData.percent
  });

  return score.save().then(saved => {
    res.status(200).json({ success: true });
  });
}

function ping(req, res) {
  console.log("get /api/ping/");
  res.json({ status: true });
}

module.exports = {
  getSongsList,
  getSongMeta,
  setScore,
  ping
};
