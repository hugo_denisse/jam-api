const controller = require("./api.controller");
const auth = require("./auth.controller");
const decodeJwtMiddleware = require("../middlewares/decodeJwt");
const { JWT_SECRET } = require("../config/auth");

module.exports = function(app) {
  // public routes
  app.get("/api/ping", controller.ping);
  app.get("/api/songs", controller.getSongsList);
  app.get("/api/song/:name", controller.getSongMeta);
  app.use("/api/auth", auth);

  // private routes
  app.use(decodeJwtMiddleware(JWT_SECRET));
  app.post("/api/score", controller.setScore);
};
