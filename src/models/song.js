const mongoose = require("mongoose");

module.exports = mongoose.model("Song", {
  uuid: String,
  name: String,
  midi: String,
  mp3: String,
  channel: Array,
  created_at: { type: Date, default: Date.now },
  visible: { Boolean, default: false }
});
