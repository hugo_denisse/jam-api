const mongoose = require("mongoose");

module.exports = mongoose.model("Score", {
  user_uuid: String,
  song_uuid: String,
  created_at: { type: Date, default: Date.now },
  score: Number,
  streak: Number,
  percent: Number
});
