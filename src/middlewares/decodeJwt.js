const jwt = require("jsonwebtoken");

/**
 * Decodes token from req.token into req.user
 * @param {String} jwtSecret secret used to encode/decode the token
 * @param {Boolean} throwIfMissing throw error if token is not provided
 * @return {Function} middleware
 */
function setup(jwtSecret, throwIfMissing = true) {
  /**
   * Middleware
   * @param  {Object}   req  Express request
   * @param  {Object}   res  Express response
   * @param  {Function} next Express next handler
   * @returns {void}
   */
  return function middleware(req, res, next) {
    if (!req.token && throwIfMissing) {
      throw new Error("Missing JWT");
    }
    req.user = jwt.verify(req.token, jwtSecret);
    return next();
  };
}

module.exports = setup;
