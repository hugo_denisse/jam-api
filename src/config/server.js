const express = require("express");
const bodyParser = require("body-parser");
const server = require("../server");
const jwtMiddleware = require("../middlewares/jwt");
const errorHandler = require("../middlewares/errorHandler");

module.exports = () => {
  const app = express();
  app.use(bodyParser.json());
  app.use(jwtMiddleware());
  server(app);
  app.use(errorHandler);
  return app;
};
