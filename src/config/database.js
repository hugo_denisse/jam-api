const mongoose = require("mongoose");

const dbName = process.env.DB_NAME || "jam";

module.exports = () => {
  mongoose
    .connect(`mongodb://localhost/${dbName}`, {
      keepAlive: 120,
      reconnectTries: Number.MAX_VALUE,
      reconnectInterval: 1000
    })
    .then(() => {
      console.log("connected to db successfully");
    })
    .catch(err => {
      console.error("could not connect to db", { err });
    });
};
