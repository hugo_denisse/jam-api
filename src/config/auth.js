module.exports = {
  TOCTOC_URL: process.env.TOCTOC_URL || "http://localhost:8080",
  JWT_SECRET: process.env.JWT_SECRET || "test"
};
